package com.tam.coffee;

import android.widget.ImageView;
import android.widget.TextView;

public class CoffeeListItemViewHolder {
	public TextView coffeeTitleTextView;
    public TextView coffeeDescriptionTextView;
    public ImageView imageView;
}

package com.tam.coffee;

import java.io.IOException;
import com.google.api.client.util.Key;
import org.codehaus.jackson.JsonGenerationException;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Coffee {
	@Key
	public String id;
	@Key
	public String name;
	@Key
	public String desc;
	@Key
	public String image_url;
	@Key
	public String last_updated_at;
	
	
	public Coffee(String id) {
		super();
		this.id = id;
	}

	public Coffee(){
		super();
	}
	
	

	@Override
	public String toString() {
		return "Coffee [id=" + id + ", name=" + name + ", desc=" + desc
				+ ", image_url=" + image_url + ", last_updated_at="
				+ last_updated_at + "]";
	}

	public static Coffee getCoffeeFromJSON(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();
		Coffee thisCoffee = null;
		try {

			// read from the string, convert it to coffee object
			thisCoffee = mapper.readValue(jsonString, Coffee.class);
			

		} catch (JsonGenerationException e) {

			Log.d("JsonGenerationException for Coffee", e.getMessage());

		} catch (JsonMappingException e) {

			Log.d("JsonMappingException for Coffee", e.getMessage());

		} catch (IOException e) {

			Log.d("IOException for Coffee", e.getMessage());

		} 
		
		return thisCoffee;
	}

	public String toJSON(){
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = null;
		try
		{
			
			jsonString = mapper.writeValueAsString(this);
			Log.d("JSON Coffee" , jsonString);
		} catch (JsonProcessingException e) {
			Log.d("JsonProcessingException for Coffee", e.getMessage());
		}		
		return jsonString; 
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Coffee other = (Coffee) obj;
		if (desc == null) {
			if (other.desc != null) {
				return false;
			}
		} else if (!desc.equals(other.desc)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (image_url == null) {
			if (other.image_url != null) {
				return false;
			}
		} else if (!image_url.equals(other.image_url)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	
}

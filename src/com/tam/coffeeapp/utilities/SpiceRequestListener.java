package com.tam.coffeeapp.utilities;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tam.coffee.Coffee;
import com.tam.coffeeapp.CoffeeListActivity;
import com.tam.coffeeapp.CoffeeListAdapter;
import com.tam.coffeeapp.R;


public class SpiceRequestListener implements RequestListener<String> {

	private Context context;
	private Class<?> clazz;
	public SpiceRequestListener(Context context, Class <?> clazz){
		this.context = context;
		this.clazz = clazz;
	}
	
	@Override
	public void onRequestFailure( SpiceException spiceException ) {
		Log.d("Loading Failed...", spiceException.getMessage());
		Toast.makeText( context , "failure", Toast.LENGTH_SHORT ).show();
	}

	@Override
	public void onRequestSuccess( final String jsonResults ) {
		Toast.makeText( context, "success", Toast.LENGTH_SHORT ).show();
		Intent intent = new Intent(this.context,clazz);
		intent.putExtra("jsonResults", jsonResults);
		context.startActivity(intent);
		
	}

}

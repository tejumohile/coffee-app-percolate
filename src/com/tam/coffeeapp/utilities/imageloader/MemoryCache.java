package com.tam.coffeeapp.utilities.imageloader;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;



public class MemoryCache implements Cache {

	private Map<String, SoftReference<Bitmap>> cache=Collections.synchronizedMap(new HashMap<String, SoftReference<Bitmap>>());
	 
    
    public void put(String id, Bitmap bitmap){
        cache.put(id, new SoftReference<Bitmap>(bitmap));
    }
 	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		cache.clear();
	}
	
	// Returns a Bitmap
	@Override
	public Object getCacheEntry(String url) {
		if(!cache.containsKey(url))
            return null;
        SoftReference<Bitmap> ref=cache.get(url);
        return ref.get();
	}

}

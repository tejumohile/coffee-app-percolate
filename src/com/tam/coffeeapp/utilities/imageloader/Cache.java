package com.tam.coffeeapp.utilities.imageloader;

public interface Cache {

	public void clear() ;
	public Object getCacheEntry(String url);
}

package com.tam.coffeeapp.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.util.Log;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.json.jackson.JacksonFactory;
//import com.google.api.client.json.jackson.JacksonFactory;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class RoboSpiceRequestHandler extends GoogleHttpClientSpiceRequest<String>{

	private String baseURL = null;
	public RoboSpiceRequestHandler(String subStringURL) {
		super(String.class);
		// TODO Auto-generated constructor stub
		baseURL = CoffeeAPI.API_URL;
		baseURL = String.format(baseURL+subStringURL+"/?api_key="+CoffeeAPI.API_KEY);
//		baseURL = String.format(baseURL+subStringURL);
		Log.d("baseURL", baseURL);
	}

	@Override
	public String loadDataFromNetwork() {
		// TODO Auto-generated method stub
		try
		{	
			Log.d( "Call web service " , baseURL);
	        HttpRequest request = getHttpRequestFactory()
	                .buildGetRequest( new GenericUrl( baseURL ) );
	        
	        
	        //Setting the API key in the header
	        HttpHeaders headers = request.getHeaders();
	        headers.put("api_key",CoffeeAPI.API_KEY);
	        
	        request.setParser( new JacksonFactory().createJsonObjectParser() );
	        HttpResponse response = request.execute();
	        Log.d("Response Status" , response.getStatusMessage());
	     
	        
	        String jsonString = convertStreamToString(response.getContent());
	        
	        Log.d("Response String", jsonString);
	     
	        return jsonString;
			
		}
		catch (Exception e){
			Log.d("RoboSpice Exception", e.getMessage());
		}
		return null;
	}
	
	

	private static String convertStreamToString(InputStream is) {

	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append((line + "\n"));
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
	
}

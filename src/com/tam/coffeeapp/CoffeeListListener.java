package com.tam.coffeeapp;

import java.util.List;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.tam.coffee.Coffee;
import com.tam.coffeeapp.utilities.RoboSpiceRequestHandler;
import com.tam.coffeeapp.utilities.SpiceRequestListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class CoffeeListListener implements OnItemClickListener {

	private Context context;
	private List<Coffee> coffeeList;
	private Class<?> clazz; // This is the target Activity class.
	private RoboSpiceRequestHandler roboSpiceHandler;
	public CoffeeListListener(Context context, List<Coffee> coffeeList){
		this.context = context;
		this.coffeeList = coffeeList;
		
	}
	
	public void setTargetActivity(Class<?> clazz){
		this.clazz = clazz;
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Coffee coffee = coffeeList.get(position);
		//To make the call with the selected id
		Log.d("Clicked coffee", String.valueOf(position));
		Intent intent = new Intent(this.context,clazz);
		intent.putExtra("coffee", coffee.toJSON());
		context.startActivity(intent);

	}

}

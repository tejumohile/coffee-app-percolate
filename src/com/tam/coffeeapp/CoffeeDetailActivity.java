package com.tam.coffeeapp;

import java.io.IOException;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tam.coffee.Coffee;
import com.tam.coffeeapp.utilities.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CoffeeDetailActivity extends Activity {

	private String jsonResults;
	private TextView nameTextView;
	private TextView descrTextView;
	private ImageView imgView;
	private TextView updatedTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_coffee_detail);

		nameTextView = (TextView) findViewById(R.id.coffee_name);
		descrTextView = (TextView) findViewById(R.id.coffee_descr);
		updatedTextView = (TextView) findViewById(R.id.coffee_updates);
		imgView = (ImageView) findViewById(R.id.coffee_detail_img);
		Intent intent = getIntent();
		jsonResults = intent.getStringExtra("coffee");
		
		Log.d("Coffee detail json", jsonResults);
		
		setElements();
	}

	

	private Coffee getCoffeeFromJson() throws JsonParseException,
			JsonMappingException, IOException {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonResults, Coffee.class);

	}
	
	private void setElements(){
		try
		{
			Coffee coffee = getCoffeeFromJson();
			Log.d("Coffee object", coffee.toString());
			//Setting the view elements
			nameTextView.setText(coffee.name);
			descrTextView.setText(coffee.desc);
			if(coffee.last_updated_at != null 
					|| coffee.last_updated_at != "")
				updatedTextView.setText("Updated " + coffee.last_updated_at);
			if(coffee.image_url != null 
					|| coffee.image_url != "")
			{
				Log.d("Fetching the image at", coffee.image_url);
				ImageLoader loader = new ImageLoader(this);
				int imgLoader = R.drawable.ic_launcher;
				loader.DisplayImage(coffee.image_url, imgLoader, imgView);
			}
		}
		catch(Exception e){
			Log.d("Coffee Detail Exception", e.getMessage());
		}
	}

}

package com.tam.coffeeapp;

//import android.support.v7.app.ActionBarActivity;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.tam.coffeeapp.utilities.RoboSpiceRequestHandler;
import com.tam.coffeeapp.utilities.SpiceRequestListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class LaunchActivity extends BaseActivity {

	private RoboSpiceRequestHandler roboSpiceHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        roboSpiceHandler = new RoboSpiceRequestHandler("api/coffee");
        
        
    }
    
    @Override
	protected void onStart() {
		super.onStart();
		getSpiceManager().execute( roboSpiceHandler, "json",
        		2 * DurationInMillis.ONE_MINUTE,
        		new SpiceRequestListener(this,CoffeeListActivity.class));
		
	}


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.launch, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}

package com.tam.coffeeapp;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import com.tam.coffee.Coffee;
import com.tam.coffee.CoffeeListItemViewHolder;
import com.tam.coffeeapp.utilities.imageloader.ImageLoader;

public class CoffeeListAdapter extends ArrayAdapter<Coffee>{
	private final Context context;
	private final List<Coffee> coffees;
	private final ImageLoader imgLoader;
	
	public CoffeeListAdapter(Context context, 
			List<Coffee> coffees) {
		super(context, R.layout.list_item_coffee,  coffees);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.coffees = coffees;
		imgLoader = new ImageLoader(this.context);
	}
	
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("coffee_list_setting_adapter", "In Progress");
		LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView;

	      rowView = inflater.inflate(R.layout.list_item_coffee, null);
	      Log.d("coffee_list_setting_adapter", "Inflater Created");
	    CoffeeListItemViewHolder viewHolder = new CoffeeListItemViewHolder();
	    
	    setCoffeeViewHolder(rowView, viewHolder);
	    
	    rowView.setTag(viewHolder);
	    Coffee coffee = coffees.get(position);
	    viewHolder.coffeeTitleTextView.setText(coffee.name);
	    viewHolder.coffeeDescriptionTextView.setText(coffee.desc);
//	    int loader = R.drawable.ic_launcher;
	    int loader = 0;
	    if (coffee.image_url != null || coffee.image_url != "") {
	    	// If the Image URL exists then fetch the URL
	    	imgLoader.DisplayImage(coffee.image_url, loader, viewHolder.imageView);
	    }
	    

	    return rowView;
	  }

	private void setCoffeeViewHolder(View rowView, CoffeeListItemViewHolder viewHolder) {
		viewHolder.coffeeTitleTextView = (TextView) rowView.findViewById(R.id.coffee_title);
	    viewHolder.coffeeDescriptionTextView = (TextView) rowView.findViewById(R.id.coffee_description);   
	    viewHolder.imageView = (ImageView) rowView.findViewById(R.id.coffee_img);

	}
	
}

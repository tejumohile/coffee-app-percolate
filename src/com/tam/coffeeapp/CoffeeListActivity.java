package com.tam.coffeeapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tam.coffee.Coffee;
import com.tam.coffeeapp.utilities.RoboSpiceRequestHandler;
import com.tam.coffeeapp.utilities.SpiceRequestListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class CoffeeListActivity extends BaseActivity{
	private ListView coffeeListView;
	private String jsonResults;
	private static List<Coffee> coffeeList ;
	private CoffeeListListener listener;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_coffee);
        coffeeListView = (ListView) findViewById(R.id.coffee_list);
        
        Intent intent = getIntent();        
        jsonResults = intent.getStringExtra("jsonResults");        
       
        
        
    }
	
	@Override
    protected void onStart() {
		super.onStart();
		displayList();
	}
	
	private void displayList(){
		
		try{

	        coffeeList = getCoffeeFromJsonList(jsonResults);
	        
	        if(coffeeList != null) {
	        	
	        	listener = new CoffeeListListener(this, coffeeList);
	            coffeeListView.setOnItemClickListener(listener);
	            listener.setTargetActivity(CoffeeDetailActivity.class);
	            CoffeeListAdapter adapter = new CoffeeListAdapter(this, coffeeList);
	            coffeeListView.setAdapter(adapter);
	        	
	        }
		}
		catch(Exception e){
			Log.d("Coffee Exception", e.getMessage());
		}
	}
	
	private List<Coffee> getCoffeeFromJsonList(String jsonString) 
			throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		Coffee [] coffees =  mapper.readValue(jsonString, Coffee[].class);
		return Arrays.asList(coffees);
	}
}

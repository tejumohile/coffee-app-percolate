package com.tam.coffeeapp;

import android.app.Activity;
import android.util.Log;

import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;
import com.octo.android.robospice.SpiceManager;

public class BaseActivity extends Activity {
	private SpiceManager spiceManager = new SpiceManager(JacksonGoogleHttpClientSpiceService.class);

	@Override
	protected void onStart() {
		spiceManager.start(this);
		super.onStart();
		Log.d("Spice Service","started = true" );
	}

	@Override
	protected void onStop() {
		if(spiceManager.getPendingRequestCount() > 0)
			spiceManager.cancelAllRequests();
		spiceManager.shouldStop();
		super.onStop();
	}

	protected SpiceManager getSpiceManager() {
		return spiceManager;
	}

}

# Android Coffee App #

This Android project is a part of an assignment submission. It uses the Coffee API at : https://coffeeapi.percolate.com

### Libraries used ###

* robospice
* jackson
* googlehttpclient

### How do I get set up? ###

* Min SDK Version : 16
* Target SDK Version : 20

### Who do I talk to? ###

* Tejashree Mohile : tejumohile@gmail.com